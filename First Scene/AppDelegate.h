//
//  AppDelegate.h
//  First Scene
//
//  Created by Brandon Levasseur on 2/12/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@import SceneKit;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (weak) IBOutlet SCNView *sceneView;

@end

