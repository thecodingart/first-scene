//
//  AppDelegate.m
//  First Scene
//
//  Created by Brandon Levasseur on 2/12/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AppDelegate.h"

const BOOL useOmniLight = NO;
const BOOL useSpotlight = !useOmniLight;
const BOOL useAmbientLight = YES;
const BOOL useTwoBoxes = NO;
const BOOL useMultipleMaterials = YES;

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)awakeFromNib
{
    SCNScene *scene = [SCNScene scene];
    self.sceneView.scene = scene;
    
    CGFloat boxSide = 10.0;
    SCNBox *box = [SCNBox boxWithWidth:boxSide height:boxSide length:boxSide chamferRadius:0];
    SCNNode *boxNode = [SCNNode nodeWithGeometry:box];
    boxNode.rotation = SCNVector4Make(0, 1, 0, M_PI/5.0);

    
    [scene.rootNode addChildNode:boxNode];
    
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    cameraNode.position = SCNVector3Make(0, 10, 20);
    cameraNode.rotation = SCNVector4Make(1, 0, 0, -atan2f(10.0, 20.0)/*-atan(camY/camZ)... rotates around an angle per the triangle's calculated angle*/);
    [scene.rootNode addChildNode:cameraNode];
    
    NSColor *lightBlueColor = [NSColor colorWithCalibratedRed:4.0/255.0 green:120.0/255.0 blue:255.0/255.0 alpha:1.0];
    
//    box.firstMaterial.diffuse.contents = lightBlueColor;
    box.firstMaterial.specular.contents = [NSColor whiteColor];
    box.firstMaterial.locksAmbientWithDiffuse = YES;
    
    if (useOmniLight) {
        SCNLight *omniLight = [SCNLight light];
        omniLight.type = SCNLightTypeOmni;
        omniLight.color = lightBlueColor;
        
        omniLight.attenuationStartDistance = 15;
        omniLight.attenuationEndDistance = 20;
        
        SCNNode *omniLightNode = [SCNNode node];
        omniLightNode.light = omniLight;
        
        [cameraNode addChildNode:omniLightNode];
    }
    
    if (useSpotlight) {
        SCNLight *spotlight = [SCNLight light];
        spotlight.type = SCNLightTypeSpot;
        spotlight.color = [NSColor whiteColor];
        
        //a narrow spot light that fades quickly along the edge
        spotlight.spotInnerAngle = 25;
        spotlight.spotOuterAngle = 30;
        
        SCNNode *spotlightNode = [SCNNode node];
        spotlightNode.light = spotlight;
        
        [cameraNode addChildNode:spotlightNode];
    }
    
    if (useAmbientLight) {
        SCNLight *ambientLight = [SCNLight light];
        ambientLight.type = SCNLightTypeAmbient;
        ambientLight.color = [NSColor whiteColor];
        
        SCNNode *ambientLightNode = [SCNNode node];
        ambientLightNode.light = ambientLight;
        
        [scene.rootNode addChildNode:ambientLightNode];
    }
    
    if (useMultipleMaterials) {
        SCNMaterial *greenMaterial = [SCNMaterial material];
        greenMaterial.diffuse.contents = [NSColor greenColor];
        greenMaterial.locksAmbientWithDiffuse = YES;
        
        SCNMaterial *redMaterial = [SCNMaterial material];
        redMaterial.diffuse.contents = [NSColor redColor];
        redMaterial.locksAmbientWithDiffuse = YES;
        
        SCNMaterial *blueMaterial = [SCNMaterial material];
        blueMaterial.diffuse.contents = [NSColor blueColor];
        blueMaterial.locksAmbientWithDiffuse = YES;
        
        SCNMaterial *yellowMaterial = [SCNMaterial material];
        yellowMaterial.diffuse.contents = [NSColor yellowColor];
        yellowMaterial.locksAmbientWithDiffuse = YES;
        
        SCNMaterial *purpleMaterial = [SCNMaterial material];
        purpleMaterial.diffuse.contents = [NSColor purpleColor];
        purpleMaterial.locksAmbientWithDiffuse = YES;
        
        SCNMaterial *magentaMaterial = [SCNMaterial material];
        magentaMaterial.diffuse.contents = [NSColor magentaColor];
        magentaMaterial.locksAmbientWithDiffuse = YES;
        
        box.materials = @[greenMaterial, redMaterial, blueMaterial, yellowMaterial, purpleMaterial, magentaMaterial];
    }
    
    if (useTwoBoxes) {
        cameraNode.position = SCNVector3Make(0, 0, 20);
        
        SCNVector4 noRotation = SCNVector4Make(1, 0, 0, 0);
        
        cameraNode.rotation = noRotation;
        boxNode.rotation = noRotation;
        
        SCNBox *anotherBox = [SCNBox boxWithWidth:boxSide height:boxSide length:boxSide chamferRadius:0.0];
        
        anotherBox.firstMaterial.diffuse.contents = lightBlueColor;
        anotherBox.firstMaterial.locksAmbientWithDiffuse = YES;
        anotherBox.firstMaterial.specular.contents = [NSColor whiteColor];
        
        SCNNode *anotherBoxNode = [SCNNode nodeWithGeometry:anotherBox];
        [scene.rootNode addChildNode:anotherBoxNode];
        
        CGFloat boxMargin = 1.0;
        CGFloat boxPositionX = (boxSide + boxMargin)/2.0;
        boxNode.position = SCNVector3Make(boxPositionX, 0, 0);
        anotherBoxNode.position = SCNVector3Make(-boxPositionX, 0, 0);
    }

}

@end
